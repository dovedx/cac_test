"""
Created on Sat Oct  9 19:23:53 2021
#----------------vif---------------------# 
#Ref: https://github.com/abhinaukumar/vif
#----------------awds---------------------# 
#Ref: https://github.com/dinglijay/UnifiedIQA
#----------------SSIM,MS_SSIM,FSIM,CW_SSIM,GMSD,NLPD,MAD,LPIPS,DISTS---------------------# 
#Ref: https://github.com/dingkeyan93/IQA-optimization
#----------------iwssim---------------------# 

PSNR: the larger value, the quality is better
SSIM: 0-1  the larger value, the quality is better, range:0-1
vif: 0-1  the larger value, the quality is better, range: uint8
msssim: 0-1  the larger value, the quality is better, range:0-1
fssim: 0-1  the larger value, the quality is better, range:0-1
iwssim: 0-1  the larger value, the quality is better, range: uint8
aws: 0-1  the larger value, the quality is better

gmsd: 0-1  the lower value, the quality is better, range:0-1
nlpd: 0-1  the lower value, the quality is better, range:0-1
mad: the lower value, the quality is better, range:0-1
DISTS: 0-1  the lower value, the quality is better, range:0-1
LPIPSvgg: 0-1  the lower value, the quality is better, range:0-1
@author: blchen
"""

import numpy as np
import numpy
import torch
from IQA_pytorch import FSIM,GMSD,NLPD,MAD,LPIPSvgg,DISTS,SSIM, MS_SSIM 
from skimage.measure import compare_ssim
from pytorch_msssim import ssim, ms_ssim

from  utils.IWSSIM import IW_SSIM
import os
import cv2
from PIL import Image
from utils.vif_utils import vif
from  utils.awds import AWDS
import argparse
import math
from torchvision import transforms

from collections import OrderedDict
from datetime import datetime
import json
import re
import glob

from PIL import Image

def parse(opt_path, is_train=True):
    json_str = ''
    with open(opt_path, 'r') as f:
        for line in f:
            line = line.split('//')[0] + '\n'
            json_str += line

    opt = json.loads(json_str, object_pairs_hook=OrderedDict)
    
    return opt


def yuv_import_8_bits(filename, dims ,startfrm,numframe,save_ok=False):
    fp = open(filename, 'rb')
    frame_size = numpy.prod(dims) * 3 / 2
    fp.seek(0, 2)
    ps = fp.tell()
    totalfrm = int(ps // frame_size) 
#     print('8bit frames:',totalfrm)
    d00 = dims[0] // 2
    d01 = dims[1] // 2
#     assert startfrm+numframe<=totalfrm
    if startfrm+numframe>=totalfrm:
        numframe=totalfrm-1
    Y = numpy.zeros(shape=(numframe, 1,dims[0], dims[1]), dtype=numpy.uint8, order='C')
    U = numpy.zeros(shape=(numframe, 1,d00, d01),dtype= numpy.uint8, order='C')
    V = numpy.zeros(shape=(numframe, 1,d00, d01),dtype= numpy.uint8, order='C')
    
    fp.seek(int(frame_size * startfrm), 0)
    if save_ok==True:
        image_y = numpy.zeros(shape=(dims[0], dims[1]), dtype=numpy.uint8, order='C')
        save_dir = './yuv_image_test/yuv_ori_Y/'+filename.split('/')[-1].split('.')[0]
        os.makedirs(save_dir,exist_ok=True)
        
    for i in range(startfrm,startfrm+numframe):
        for m in range(dims[0]):
            for n in range(dims[1]):
                pel8bit = int.from_bytes(fp.read(1), byteorder='little',signed=False)
                Y[i-startfrm,0, m, n] = np.uint8(pel8bit)
                if save_ok==True:
                    image_y[m,n] = pel8bit
        if save_ok==True:
            cv2.imwrite(save_dir+'/'+'%04d.png'%(i),numpy.uint8(image_y))
    
    
        for m in range(d00):
            for n in range(d01):
                pel8bit = int.from_bytes(fp.read(1), byteorder='little',signed=False)
                U[i-startfrm,0, m, n] = np.uint8(pel8bit)                
        for m in range(d00):
            for n in range(d01):
                pel8bit = int.from_bytes(fp.read(1), byteorder='little',signed=False)
                V[i-startfrm,0, m, n] = np.uint8(pel8bit)
                
    fp.close()
    Y = Y.astype(numpy.float32)
    U = U.astype(np.float32)
    V = V.astype(np.float32)
    return Y, U, V

def yuv_import_10_bits(filename, dims ,startfrm,numframe,save_ok=False):
    fp = open(filename, 'rb')
    frame_size = numpy.prod(dims) * 3 
    
    fp.seek(0, 2)
    ps = fp.tell()
    totalfrm = int(ps // frame_size) 
    
#     print('10bit frames:',totalfrm)
    if startfrm+numframe>=totalfrm:
        
        numframe=totalfrm-1
    
    d00 = dims[0] // 2
    d01 = dims[1] // 2
    #assert startfrm+numframe<=totalfrm
    Y = numpy.zeros(shape=(numframe, 1,dims[0], dims[1]), dtype=numpy.uint16, order='C')
    U = numpy.zeros(shape=(numframe, 1,d00, d01),dtype= numpy.uint16, order='C')
    V = numpy.zeros(shape=(numframe, 1,d00, d01),dtype= numpy.uint16, order='C')
    
    fp.seek(int(frame_size * startfrm), 0)
    if save_ok==True:
        image_y = numpy.zeros(shape=(dims[0], dims[1]), dtype=numpy.uint16, order='C')
        save_dir = './yuv_image_test/yuv_LD37_Y/'+filename.split('/')[-1].split('.')[0]
        os.makedirs(save_dir,exist_ok=True)
    
    for i in range(startfrm,startfrm+numframe):
        for m in range(dims[0]):
            for n in range(dims[1]):
                pel10bit =  int.from_bytes(fp.read(2), byteorder='little', signed=False)     
                Y[i-startfrm,0, m, n] = pel10bit 
                if save_ok==True:
                    image_y[m,n] =pel10bit
                    
        if save_ok:
            image_y = (image_y/1023.)*255.0
            cv2.imwrite(save_dir+'/'+'%04d.png'%(i),numpy.uint8(image_y))
                
        for m in range(d00):
            for n in range(d01):
                pel10bit_u = int.from_bytes(fp.read(2), byteorder='little', signed=False)
                U[i-startfrm,0, m, n]= pel10bit_u 
                
        for m in range(d00):
            for n in range(d01):
                pel10bit_v = int.from_bytes(fp.read(2), byteorder='little', signed=False)
                V[i-startfrm,0, m, n]  = pel10bit_v 


    fp.close()
    Y = Y.astype(numpy.float32)
    U = U.astype(np.float32)
    V = V.astype(np.float32)
    return Y, U, V


def prepare_image(image, resize = False, repeatNum = 1):
    image= np.array(image).astype(np.uint8)
    if resize and min(image.size)>256:
        image = transforms.functional.resize(image,256)

    image = transforms.ToTensor()(image)

    return image.unsqueeze(0).repeat(repeatNum,1,1,1)

def psnr(img1, img2):
    
    mse = np.mean( (img1/255. - img2/255.) ** 2 )
    if mse < 1.0e-10:
        return 100
    PIXEL_MAX = 1
    return 20 * math.log10(PIXEL_MAX / math.sqrt(mse))

import tqdm

def Image_Assess(dec_images,ori_images,ori_bit_depth,Algorithm_set,device,RGB_mode=False,file=None,verbose=False):
    if RGB_mode:
#         ssim = SSIM(channels=3).to(device)
#         msssim = MS_SSIM(channels=3).to(device)
        fsim = FSIM(channels=3).to(device)
    else:
        ssim = SSIM(channels=1).to(device)
        msssim = MS_SSIM(channels=1).to(device)
        fsim = FSIM(channels=1).to(device)
        
    gmsd =  GMSD(channels=1).to(device)
    nlpd = NLPD(channels=1).to(device)
    mad = MAD(channels=1).to(device)
    iwssim = IW_SSIM()                
    dists = DISTS().to(device)  
    lpipsvgg = LPIPSvgg().to(device)
    
    
    length = dec_images.shape[0]
    gmsd_scores   = []
    awds_scores      = []
    psnr_scores   = []
    ssim_scores   = []
    vif_scores     = []
    msssim_scores = []
    fsim_scores   = []
    nlpd_scores   = []
    mad_scores    = []
    iwssim_scores = []
    DS_scores     = []
    LP_scores     = []

    for i in tqdm.tqdm(range(length)):
        
        dec_image = np.squeeze(dec_images[i])
        ori_image = np.squeeze(ori_images[i])
        if ori_bit_depth==10:
            ori_image =(ori_image/1023.)*255.0
        dec_image =(dec_image/1023.)*255.0
        print(dec_image.shape,ori_image.shape)
#         dec_image = Image.open('./data/test_image/dists/I04.BMP').convert("L")
#         ori_image = Image.open('./data/test_image/refs/I04.BMP').convert("L")
        
        dec_image_gpu  = prepare_image(dec_image,repeatNum=1).to(device)#range:0-1
        ori_image_gpu  = prepare_image(ori_image,repeatNum=1).to(device)#range:0-1
        ori_image_gpu.requires_grad_(False)
#         print("======>:",ori_image_gpu)
        
        dec_image_np = np.array(dec_image).astype(np.float32)#range:0-255.0
        ori_image_np = np.array(ori_image).astype(np.float32)#range:0-255.0
        img_dec_torch       = torch.from_numpy(dec_image_np[np.newaxis,np.newaxis,...]).to(device)   # 1, C, H, W
        img_ori_torch       = torch.from_numpy(ori_image_np[np.newaxis,np.newaxis,...]).to(device)

        if RGB_mode:
            #gmsd必须要3通道输入
            print("support soon ...")
        else:
            if "awds" in Algorithm_set:
                if verbose:
                    print("deal with  awds ...")
                awds_score = AWDS(ori_image_np.astype(np.uint8),dec_image_np.astype(np.uint8))
            else:
                awds_score = 0
            awds_scores.append(awds_score)
            
            if "psnr" in Algorithm_set:#range:0-255
                if verbose:
                    print("deal with  psnr ...")
                psnr_score = psnr(dec_image_np,ori_image_np)
            else:
                psnr_score = 0
            psnr_scores.append(psnr_score)
            
            if "ssim" in Algorithm_set:#range:0-1
                if verbose:
                    print("deal with  ssim ...")
#                 ssim_score = ssim(dec_image_gpu, ori_image_gpu, as_loss=False).cpu().item()
                
                ssim_score = compare_ssim((dec_image_np),(ori_image_np),data_range=255)#输入数据范围:0-255
#                 print(dec_image_np[100:105,100:105])
            else:
                ssim_score = 0
            ssim_scores.append(ssim_score)
            
            if "vif" in Algorithm_set:#range(0-255)
                if verbose:
                    print("deal with  vif ...")
                vif_score = vif(ori_image_np,dec_image_np)
            else:
                vif_score = 0
            vif_scores.append(vif_score)
            
            if "msssim" in Algorithm_set:#
                if verbose:
                    print("deal with  msssim ...")
#                 msssim_score = msssim(dec_image_gpu, ori_image_gpu , as_loss=False).cpu().item()#输入数据范围0-1               
                msssim_score   = ms_ssim(img_dec_torch, img_ori_torch, win_size=11, data_range=255).cpu().item()#输入数据范围0-255
            else:
                msssim_score = 0
            msssim_scores.append(msssim_score)
            
            if "fsim" in Algorithm_set:#range:0-1
                if verbose:
                    print("deal with fsim ...")
                fsim_score = fsim(ori_image_gpu, dec_image_gpu, as_loss=False).cpu().item()
            else:
                fsim_score = 0
            fsim_scores.append(fsim_score)
            
            if "nlpd" in Algorithm_set:
                if verbose:
                    print("deal with npld ...")
                nlpd_score = nlpd(dec_image_gpu, ori_image_gpu, as_loss=False).cpu().item()
            else:
                nlpd_score = 0
            nlpd_scores.append(nlpd_score)
            
            if "mad" in Algorithm_set:#smaller is better
                if verbose:
                    print("deal with mad...")
                mad_score = mad(ori_image_gpu, dec_image_gpu, as_loss=False).cpu().item()
            else:
                mad_score = 0
            mad_scores.append(mad_score)
            
            if "iwssim" in Algorithm_set:#larger is better
                if verbose:
                    print("deal with iwssim...")
                iwssim_score = iwssim(ori_image_np,dec_image_np, as_loss=False).item()
            else:
                iwssim_score = 0
            iwssim_scores.append(iwssim_score)
            
            if "gmsd" in Algorithm_set:   
                if verbose:
                    print("deal with gmsd...")                        
                gmsd_score = gmsd(ori_image_gpu, dec_image_gpu, as_loss=False).cpu().item()
            else:
                gmsd_score = 0
            gmsd_scores.append(gmsd_score)
#             print('GMSD score: %.4f' % gmsd_score, file=file, flush=True)
            
            if "dists" in Algorithm_set:
                if verbose:
                    print("deal with dists...")
                DS_score = dists(ori_image_gpu, dec_image_gpu, as_loss=False).cpu().item()
            else:
                DS_score = 0
            DS_scores.append(DS_score)
            
            if "lpips" in Algorithm_set:
                if verbose:
                    print("deal with  lpips ...")
                LP_score = lpipsvgg(ori_image_gpu, dec_image_gpu, as_loss=False).cpu().item()
            else:
                LP_score = 0
            LP_scores.append(LP_score)

#     return awds_scores,psnr_scores,ssim_scores,vif_scores,msssim_scores,fsim_scores,nlpd_scores,mad_scores,iwssim_scores,gmsd_scores,DS_scores,LP_scores
    return psnr_scores,ssim_scores,msssim_scores,vif_scores,gmsd_scores,mad_scores,fsim_scores,iwssim_scores,awds_scores,nlpd_scores,DS_scores,LP_scores

def txt_init(file):
    print('**********'*4,'Quality evaluation','**********'*4,file=file, flush=True)
    
#     print("{:<30}{:<15}{:<15}{:<15}{:<15}{:<15}{:<15}{:<15}{:<15}{:<15}{:<15}{:<15}{:<15}".format('video_name','awds_scores','psnr_scores', 'ssim_scores', 'vif_scores' ,'msssim_scores' ,'fsim_scores' ,'nlpd_scores', 'mad_scores', 'iwssim_scores','gmsd_scores', 'Dists_scores', 'Lipis_scores'),file=file,flush=True)
    print("{:<30}{:<15}{:<15}{:<15}{:<15}{:<15}{:<15}{:<15}{:<15}{:<15}{:<15}{:<15}{:<15}".format('video_name','psnr_scores','ssim_scores','msssim_scores', 'vif_scores' ,'gmsd_scores' ,'mad_scores' ,'fsim_scores', 'iwssim_scores', 'awds_scores','nlpd_scores', 'Dists_scores', 'Lpips_scores'),file=file,flush=True)
    
            
def Image_Assess_log(filename,QA_data,QA_type,file,verbose=False):
    QA_length = len(QA_data)

#     print("-----","awscores psnr_scores ssim_scores vifscores msssim_scores fsim_scores nlpd_scores mad_scores iwssim_scores DS_scores LP_scores","-----",file=file, flush=True)
    
#     print("{:<30}".format(filename.split('/')[-1]),end='',file=file, flush=True)
#     for i in range(QA_length):
#         QA_list = QA_data[i]            
#         value = np.mean(QA_list)
#         if verbose:
#             print("{:.4f}  ".format(value))
#         print("{:<13.4f}  ".format(value) ,end='',file=file, flush=True)
    
#     print("{:<15}".format(QA_type),file=file, flush=True)
    
    for i in range(len(QA_data[0])):
        print("{:<30}".format(i),end='',file=file, flush=True)
        for j in range(QA_length):
            
            value = QA_data[j][i]
            print("{:<13.4f}  ".format(value) ,end='',file=file, flush=True)
            
        print("{:<15}".format(QA_type),file=file, flush=True)
    
    print("{:<30}".format(filename.split('/')[-1]),end='',file=file, flush=True)
    for i in range(QA_length):
        QA_list = QA_data[i]            
        value = np.mean(QA_list)
        if verbose:
            print("{:.4f}  ".format(value))
        print("{:<13.4f}  ".format(value) ,end='',file=file, flush=True)
    
    print("{:<15}".format(QA_type),file=file, flush=True)

    
def IQA_YUV(ori_yuv_path,dec_yuv_path,startfrm,numfrm,yuv_h_w,ori_bit_depth,Algorithm_set,yuv_mode,save_ok,log_file,RGB_mode,device,verbose):
    if ori_bit_depth==8:
        Y_ori,U_ori,V_ori = yuv_import_8_bits(filename=ori_yuv_path,  dims= yuv_h_w, startfrm=startfrm, numframe=numfrm, save_ok=save_ok)
    elif ori_bit_depth==10:
        Y_ori,U_ori,V_ori = yuv_import_10_bits(filename=ori_yuv_path,  dims= yuv_h_w, startfrm=startfrm, numframe=numfrm, save_ok=save_ok)
        
    Y_dec,U_dec,V_dec = yuv_import_10_bits(filename=dec_yuv_path , dims=yuv_h_w ,startfrm=startfrm ,numframe=numfrm ,save_ok=save_ok)
    
    
    os.makedirs(log_file.rsplit('/',1)[0],exist_ok=True)
    f_file = open(log_file,"w")
    
    txt_init(file=f_file)
    if "Y" in yuv_mode:
        Y_QA=Image_Assess(dec_images=Y_dec,ori_images=Y_ori,ori_bit_depth=ori_bit_depth,Algorithm_set=Algorithm_set,device=device,RGB_mode=False,file=f_file,verbose=verbose)
        Image_Assess_log(filename=ori_yuv_path, QA_data=Y_QA,QA_type='Y_channel',file=f_file,verbose=True)
    if "U" in yuv_mode:
        U_QA=Image_Assess(dec_images=U_dec,ori_images=U_ori,ori_bit_depth=ori_bit_depth,device=device,Algorithm_set=Algorithm_set,RGB_mode=False,file=f_file,verbose=verbose)
        Image_Assess_log(filename=ori_yuv_path, QA_data=U_QA,QA_type='U_channel',file=f_file,verbose=True)
    if "V" in yuv_mode:
        V_QA=Image_Assess(dec_images=V_dec,ori_images=V_ori,ori_bit_depth=ori_bit_depth,device=device,Algorithm_set=Algorithm_set,RGB_mode=False,file=f_file,verbose=verbose)
        Image_Assess_log(filename=ori_yuv_path, QA_data=V_QA,QA_type='V_channel',file=f_file,verbose=True)
    
    f_file.close()
    print("IQA_YUV finished ...")
    
    
if __name__=="__main__":
    
    opt_path = '/userhome/share_hosts/IQA_codec/CAC_EVE/options/config.json'
    opt      = parse(opt_path=opt_path)

    device   = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    dec_yuv_path = opt["image"]["dec_yuv_video_path"]
    ori_yuv_path = opt["image"]["ori_yuv_video_path"]

    yuv_h_w      = opt["image"]["yuv_dim"]
    startfrm     = opt["image"]["startfrm"]
    numfrm       = opt["image"]["numframe"]
    log_file     = opt["image"]["image_log_file"]
    save_ok      = opt["image"]["save_ok"]
    RGB_mode     = opt["image"]["RGB_mode"]
    verbose      = opt["image"]["verbose"]
    
    IQA_YUV(ori_yuv_path=ori_yuv_path,dec_yuv_path=dec_yuv_path,startfrm=startfrm,numfrm=numfrm,yuv_h_w=yuv_h_w,save_ok=save_ok,log_file=log_file,RGB_mode=RGB_mode,device=device,verbose=verbose)

   
    
    
    
