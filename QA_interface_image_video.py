from CAC_EVE_Images_interface import *

from CAC_EVE_Videos_interface import *

import argparse


if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--use_cuda", action='store_true', help='Specify whether to use cuda or not')
    parser.add_argument("--QA_type", default=None,type=str, help='Quality Acesss type,video or image')
    parser.add_argument("--dec_yuv_video_path", default=None,type=str, help='decode video path')
    parser.add_argument("--ori_yuv_video_path", default=None,type=str, help='origin video path')
    parser.add_argument("--startfrm", default=None,type=int, help='start frame of the yuv video')
    parser.add_argument("--numfrm", default=None,type=int, help='Access frames of the yuv video')
    
    parser.add_argument("--yuv_width" , type=int, default=None)
    parser.add_argument("--yuv_height", type=int, default=None)
    parser.add_argument("--yuv_mode"  , type=list,default=None)
    parser.add_argument("--ori_bit_depth", type=int, default=None)
#     ["awds","psnr","ssim","vif","msssim","fsim","nlpd","mad","iwssim","dists"]
    

    
    arg = parser.parse_args()
    
    opt_path = '/userhome/share_hosts/IQA_codec/CAC_EVE/options/config.json'
    opt = parse(opt_path=opt_path)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    
    width = arg.yuv_width
    height= arg.yuv_height
    if width != None and height !=None:
        opt["image"]["yuv_dim"]          = [height,width]
        opt["video"]["Vmaf"]["size_dec"] = str(width)+"x"+str(height)
        opt["video"]["Vmeon"]["height"]  = height
        opt["video"]["Vmeon"]["width"]   = width
        opt["video"]["DVQA"]["height"]   = height
        opt["video"]["DVQA"]["width"]    = width
    if arg.QA_type !=None:
        opt["assess_type"] = arg.QA_type
    if arg.dec_yuv_video_path!=None:
        opt["dec_yuv_video_path"] = arg.dec_yuv_video_path
    if arg.ori_yuv_video_path!=None:
        opt["ori_yuv_video_path"] = arg.ori_yuv_video_path
    if arg.startfrm != None:
        opt["image"]["startfrm"]  = arg.startfrm
    if arg.numfrm != None:
        opt["image"]["numfrm"]    = arg.numfrm
    if arg.ori_bit_depth !=None:
        opt["ori_bit_depth"]      = arg.ori_bit_depth
    if arg.yuv_mode !=None:
        opt["yuv_mode"]           = arg.yuv_mode
        
    print(opt)
        
    QA_type = opt["assess_type"]
    ori_bit_depth=opt["ori_bit_depth"]
    if QA_type=="Image":
        print("image mode......")
        dec_yuv_path = opt["dec_yuv_video_path"]
        ori_yuv_path = opt["ori_yuv_video_path"]

        yuv_h_w      = opt["image"]["yuv_dim"]
        startfrm     = opt["image"]["startfrm"]
        numfrm       = opt["image"]["numfrm"]
        log_file     = opt["image"]["image_log_file"]
        save_ok      = opt["image"]["save_ok"]
        RGB_mode     = opt["image"]["RGB_mode"]
        verbose      = opt["image"]["verbose"]
        Algorithm_set= opt["image"]["Algorithm_set"]
        yuv_mode     = opt["image"]["yuv_mode"]
        IQA_YUV(ori_yuv_path=ori_yuv_path,dec_yuv_path=dec_yuv_path,startfrm=startfrm,numfrm=numfrm,yuv_h_w=yuv_h_w,ori_bit_depth=ori_bit_depth,Algorithm_set=Algorithm_set,yuv_mode=yuv_mode,save_ok=save_ok,log_file=log_file,RGB_mode=RGB_mode,device=device,verbose=verbose)
    
    if QA_type=="Video":
        log_file    = opt["video"]["video_log_file"]
        video_name  = opt["ori_yuv_video_path"].rsplit('/')[-1]
        
        f_file   = open(log_file,"w")
        
        VQA      = Video_QA(opt=opt,device=device,file=f_file)
        print("{:<30}".format(video_name),end='',file=f_file,flush=True)
        if opt["video"]["DVQA"]["enable"]:
            DVQA_data = VQA.VQA_DVQA()
            print("{:<13.4}".format(DVQA_data[0]),end='',file=f_file,flush=True)
        else:
            print("{:<15}".format('---'),end='',file=f_file,flush=True)

        if opt["video"]["Vmeon"]["enable"]:
            vmeon_data = VQA.VQA_vmeon()
            print("{:<13.4}".format(vmeon_data),end='',file=f_file,flush=True)
        else:
            print("{:<15}".format('---'),end='',file=f_file,flush=True)

        if opt["video"]["Vmaf"]["enable"]:
            os.makedirs("./log_vmaf",exist_ok=True)
            tool           = opt["video"]["Vmaf"]["tool"]
            size_dec       = opt["video"]["Vmaf"]["size_dec"]
            size_ori       = opt["video"]["Vmaf"]["size_ori"]
            dec_yuv_video_path   = opt["dec_yuv_video_path"]
            ori_yuv_video_path   = opt["ori_yuv_video_path"]
            dec_yuv_format = opt["video"]["Vmaf"]["format_dec"]
            if ori_bit_depth==10:
                opt["video"]["Vmaf"]["format_ori"]="yuv420p10le"
            ori_yuv_format = opt["video"]["Vmaf"]["format_ori"]
            filter_type    = opt["video"]["Vmaf"]["filter_type"]
            libvmaf        = opt["video"]["Vmaf"]["libvmaf"]
            end            = opt["video"]["Vmaf"]["end"]

            cmd_system = "{tool} -s {size_ori} -pix_fmt {ori_yuv_format} -i {ori_yuv_video_path} -s {size_dec} -pix_fmt {dec_yuv_format} -i {dec_yuv_video_path} {filter_type} {libvmaf} {end}".format(tool=tool,size_ori=size_ori,ori_yuv_format=ori_yuv_format,ori_yuv_video_path=ori_yuv_video_path,size_dec=size_dec,dec_yuv_format=dec_yuv_format,dec_yuv_video_path=dec_yuv_video_path,filter_type=filter_type,libvmaf=libvmaf,end=end)

            print("vmaf:",cmd_system,file=f_file,flush=True)
            os.system(cmd_system)

            

        f_file.close()
