质量评估接口包含图像质量评估和视频质量评估：

图像质量评估指标:非deep指标:["psnr","ssim","msssim","vif","gmsd","fsim","iwssim"],deep指标:["dists","lpips"]
视频质量评估指标：["DVQA","Vmeon","Vmaf"]

我的云脑环境已经设置成公共镜像:ffmpeg_vmaf,这个环境暂时跑VMEON算法会崩，建议关闭。
                                            VMEON在普通环境中跑就可以。

测试Image模式和Video的VMEON模式:pip install ffmpeg -i https://pypi.tuna.tsinghua.edu.cn/simple
测试Video的其他两种模式:要按下面的安装ffmpeg步骤安装ffmpeg

代码接口使用说明:
1、参数传入采用的是json文件："./options/config.json"，部分参数可以命令行输入：评估类型(image或者video)，视频路径，视频宽高，要处理的帧数，输入视频bit位。

 eg:python QA_interface_image_video.py --QA_type="Image" --dec_yuv_video_path="" --ori_yuv_video_path="" --startfrm=0 --numfrm=4 --yuv_width=1280 --yuv_height=720 --ori_bit_depth=8
 eg:python QA_interface_image_video.py --QA_type="Video" --ori_yuv_video_path="" --dec_yuv_video_path=""  --ori_bit_depth=8 
 video中的vmaf指标注意事项:1>ffmpeg通过-pix_fmt参数来判定视频格式(10bit,8bit),两个视频长度不一样，通过参数shortest=1:repeatlast=0组合,来以短的视频帧数为基准来计算vmaf.
                           2>命令行中，两个视频的传入顺序对vamf结果有影响，在image的指标中，同样要注意视频传入顺序。
                           3>视频长宽信息在config中修改

2、接口的调用可以参考QA_interface_image_video_demo.py.
3、输出的指标在log文件夹中，其中Vmaf指标用xml文件存储

4.注意vmaf的计算很依赖于ffmpeg的版本，而且由于ffmpeg引起的环境冲突，需要注意。

ffmpeg引起的环境冲突解决方案(linux环境下安装):

安装ffmpeg:
卸载调原来conda环境中的ffmpeg

step 1: wget https://johnvansickle.com/ffmpeg/builds/ffmpeg-git-amd64-static.tar.xz
step 2:  wget https://johnvansickle.com/ffmpeg/builds/ffmpeg-git-amd64-static.tar.xz.md5
step 3:  md5sum -c ffmpeg-git-amd64-static.tar.xz.md5 用于检测安装包的完整性
step 4:  tar xvf ffmpeg-git-amd64-static.tar.xz 解压
step 5：./ffmpeg-git-20210611-amd64-static/ffmpeg 测试是否安装成功

为了全局通用，需要将ffmpeg加入conda环境变量:
step 1：echo $PATH 会出现系统调用的环境路径,比如我的输出：/usr/local/nvm/versions/node/v12.6.0/bin:/opt/conda/bin:/usr/local/mpi/bin:/usr/local/nvidia/bin:/usr/local/cuda/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
step 2 : 将ffmpeg移动到/opt/conda/bin下,sudo mv /ffmpeg-git-20210611-amd64-static/ffmpeg /ffmpeg-git-20210611-amd64-static/ffprobe /opt/conda/bin
step 3：查看是否移动成功:whereis ffmpeg ,路径匹配就可。

*如果是import ffmpeg报错，则可以使用pip install ffmpeg 解决



