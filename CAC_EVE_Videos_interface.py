import os
import sys
from VMEON.VMEON_release.V_MEON import Predictor
import torch
import json
import numpy as np
import torch.nn as nn
from DVQA.dataset.dataset import VideoDataset
from DVQA.model.network import C3DVQANet
from scipy.stats import spearmanr, pearsonr
from DVQA.opts import parse_opts
from collections import OrderedDict

def parse(opt_path, is_train=True):
        json_str = ''
        with open(opt_path, 'r') as f:
            for line in f:
                line = line.split('//')[0] + '\n'
                json_str += line

        opt = json.loads(json_str, object_pairs_hook=OrderedDict)
        
        return opt

class Video_QA(object):
    def __init__(self,opt,device,file):

        self.opt = opt
        self.device = device
        self.file = file
        self.txt_init()
        
    def txt_init(self):
        
        print('**********'*4,' video Quality evaluation','**********'*4,file=self.file, flush=True)

        print("{:<30}{:<15}{:<15}{:<15}".format('video_name',"DVQA","Vmeon","Vmaf"),file=self.file,flush=True)
    
    def VQA_DVQA_init(self):
        
        load_checkpoint = self.opt["video"]["DVQA"]["load_model"]
        # MULTI_GPU_MODE = opt.multi_gpu
        channel = self.opt["video"]["DVQA"]["channel"]
        size_x = self.opt["video"]["DVQA"]["size_x"]
        size_y = self.opt["video"]["DVQA"]["size_y"]
        stride_x = self.opt["video"]["DVQA"]["stride_x"]
        stride_y = self.opt["video"]["DVQA"]["stride_y"]
        video_dataset = {x: VideoDataset(self.opt, x, channel, size_x, size_y, stride_x, stride_y) for x in ['test']}
        self.dataloaders = {x: torch.utils.data.DataLoader(video_dataset[x], batch_size=1, shuffle=False, num_workers=4, drop_last=False) for x in ['test']}

        checkpoint = torch.load(load_checkpoint)

        self.DVQA_model = C3DVQANet().to(self.device)
        self.DVQA_model.load_state_dict(checkpoint['model_state_dict'])

    def VQA_DVQA(self,file=None):
        self.VQA_DVQA_init()
        phase = 'test'
        self.DVQA_model.eval()

        epoch_labels = []
        epoch_preds = []

        for ref, dis, labels in self.dataloaders[phase]:

            ref = ref.to(self.device)
            dis = dis.to(self.device)
            labels = labels.to(self.device).float()

            # dim: [batch=1, P, C, D, H, W]
            ref = ref.reshape(-1, ref.shape[2], ref.shape[3], ref.shape[4], ref.shape[5])
            dis = dis.reshape(-1, dis.shape[2], dis.shape[3], dis.shape[4], dis.shape[5])

            with torch.no_grad():
                preds = self.DVQA_model(ref, dis)
                preds = torch.mean(preds, 0, keepdim=True)

            epoch_labels.append(labels.flatten())
            epoch_preds.append(preds.flatten())

        epoch_labels = torch.cat(epoch_labels).flatten().data.cpu().numpy()
        epoch_preds = torch.cat(epoch_preds).flatten().data.cpu().numpy()
        
        ret = {}
        ret['MOS'] = epoch_labels.tolist()
        ret['PRED'] = epoch_preds.tolist()
        
        return ret['PRED']

    def VQA_vmeon_init(self):
        self.test_vid=self.opt["video"]["Vmeon"]["test_vid"]
        self.width  = self.opt["video"]["Vmeon"]["width"]
        self.height = self.opt["video"]["Vmeon"]["height"]
        self.pix_fmt= self.opt["video"]["Vmeon"]["pix_fmt"]
        self.fps    = self.opt["video"]["Vmeon"]["fps"]
        self.vmeon_model = Predictor(self.opt)
    
    def VQA_vmeon(self):
        self.VQA_vmeon_init()
        _, predicted_score = self.vmeon_model.predict_quality(self.test_vid, width=self.width, height=self.height, pix_fmt=self.pix_fmt, fps=self.fps)

        return predicted_score


if __name__=="__main__":
    opt_path = '/userhome/share_hosts/IQA_codec/CAC_EVE/options/config.json'
    opt = parse(opt_path=opt_path)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    log_file = opt["video"]["video_log_file"]
    video_name =opt["image"]["ori_yuv_video_path"].rsplit('/')[-1]
    f_file   = open(log_file,"w")
    
    
    VQA = Video_QA(opt=opt,device=device,file=f_file)
    
    print("{:<30}".format(video_name),end='',file=f_file,flush=True)
    if opt["video"]["DVQA"]["enable"]:
        
        DVQA_score = VQA.VQA_DVQA()
        print("{:<13.4}".format(DVQA_score[0]),end='',file=f_file,flush=True)
    else:
        print("{:<15}".format('---'),end='',file=f_file,flush=True)
        
    if opt["video"]["Vmeon"]["enable"]:
        vmeon_score = VQA.VQA_vmeon()
        print("{:<13.4}".format(vmeon_score),end='',file=f_file,flush=True)
    else:
        print("{:<15}".format('---'),end='',file=f_file,flush=True)
        
    if opt["video"]["Vmaf"]["enable"]:
        print("Vmaf ......")
        tool = "ffmpeg"
        size_dec = "1920x720"
        size_ori = "1920x720"
        dec_video_path = "/userhome/share_hosts/hpm_cluster-HPM_v2/LD_2s_yuv_test/dec_Crew_LD_37_rap0.yuv"
        ori_video_path = "/userhome/share_hosts/AVS3_codec/AVS3_Seq/Crew_1280x720_60.yuv"
        filter_type="-lavfi"
        libvmaf="libvmaf=model_path=/userhome/share_hosts/IQA_codec/CAC_EVE/vmaf_models/vmaf_v0.6.1.json:log_fmt=xml:log_path=./log/2.xml"
        end="-f null -"

        cmd_system = "{tool} -s {size_dec} -i {dec_video_path} -s {size_ori} -i {ori_video_path} {filter_type} {libvmaf} {end}".format(tool=tool,size_dec=size_dec,dec_video_path=dec_video_path,size_ori=size_ori,ori_video_path=ori_video_path,filter_type=filter_type,libvmaf=libvmaf,end=end)
        # cmd="ffmpeg -s 1920x720 -i /userhome/share_hosts/hpm_cluster-HPM_v2/LD_2s_yuv_test/dec_Crew_LD_37_rap0.yuv -s 1920x720 -i /userhome/share_hosts/AVS3_codec/AVS3_Seq/Crew_1280x720_60.yuv -lavfi libvmaf=model_path=/userhome/share_hosts/IQA_codec/CAC_EVE/vmaf_models/vmaf_v0.6.1.json:log_fmt=xml:log_path=/log/ -f null -"
        # cmd="ls"
        # r=os.popen(cmd)
        print("vmaf:",cmd_system)
        os.system(cmd_system)

    
    f_file.close()